client_id="<redacted>";
client_secret="<redacted>";

res=`curl -X POST "https://accounts.spotify.com/api/token" \
     -H "Content-Type: application/x-www-form-urlencoded" \
     -d "grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}"`;

access_token=`echo $res | jq '.access_token'`;

echo "export ACCESS_TOKEN=${access_token};" > .env.local;

echo "Now run 'source .env.local'";

exit 0;
