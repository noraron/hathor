res=`curl --request GET \
  --url https://api.spotify.com/v1/recommendations/available-genre-seeds
  --header "Authorization: Bearer  ${ACCESS_TOKEN}"`;

echo $res | jq;

exit 0;
