genre="rock"

res=`curl --request GET \
  --url "https://api.spotify.com/v1/search?q=genre%3A${genre}&type=artist" \
  --header "Authorization: Bearer ${ACCESS_TOKEN}"`;

echo $res | jq '.artists.items | [ .[] | { name: .name, id: .id } ]';

exit 0;
